import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  
  constructor(
    private pokemonCatalogueService : PokemonCatalogueService,
    private router : Router
  ) { }

  onSubmit(event: any) {
    if(event.target.search.value.length > 0) {
      this.pokemonCatalogueService.searchPokemons = this.pokemonCatalogueService.allPokemons.filter((element) => {
        const matchRegularExpression = new RegExp(event.target.search.value, 'g' );
        return element.name.match(matchRegularExpression)
      })
  
      this.router.navigateByUrl("search-result");    
    }
  }
  ngOnInit(): void {
  }

}
