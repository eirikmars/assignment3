import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigatePokemonsComponent } from './navigate-pokemons.component';

describe('NavigatePokemonsComponent', () => {
  let component: NavigatePokemonsComponent;
  let fixture: ComponentFixture<NavigatePokemonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigatePokemonsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavigatePokemonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
