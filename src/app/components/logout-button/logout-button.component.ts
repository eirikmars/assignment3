import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css']
})
export class LogoutButtonComponent implements OnInit {

  constructor(
    private userService : UserService
  ) { }

  /**
   * The function onClickLogout() is a void function that deletes the user's information from the local
   * storage and sets the userService.user to undefined.
   */
  onClickLogout(): void {
    StorageUtil.storageDelete("pokemon-trainer")
    this.userService.user = undefined
  }

  ngOnInit(): void {
  }

}
