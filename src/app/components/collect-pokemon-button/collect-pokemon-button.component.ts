import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CollectedService } from 'src/app/services/collected.service';
import { UserService } from 'src/app/services/user.service';
import { style,trigger,state, transition, animate} from "@angular/animations"

@Component({
  selector: 'app-collect-pokemon-button',
  templateUrl: './collect-pokemon-button.component.html',
  styleUrls: ['./collect-pokemon-button.component.css'],
  animations: [
    trigger('collect', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateX(100px)' }),
        animate(2000,
          style({ opacity: 1, transform: 'translateX(-110px)' })
        ) 
    ])]),
    trigger('letGo', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateX(-110px)' }),
        animate(2000,
          style({ opacity: 1, transform: 'translateX(80px)' })
        ) 
    ])])
]})

export class CollectPokemonButtonComponent implements OnInit {
  public isInCollection: boolean = false;
  public animationOpen = false;
  public animationClose = false;


  @Input() pokemonName: string = "";

  get loading(): boolean {
    return this.collectedService.loading;
  }

  constructor(
    private readonly collectedService: CollectedService,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.isInCollection = this.userService.inCollection(this.pokemonName)
  }


  onCollectClick(): void {
    if (this.isInCollection){
      this.animationOpen=true
      setTimeout(() => {
        this.collectedService.updateCollection(this.pokemonName)
        .subscribe({
        next: (response: User) => {
          this.isInCollection = this.userService.inCollection(this.pokemonName)
        },
        error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message);
        }
    })
        this.animationOpen=false}, 1900)
    } else {
      this.animationClose = true;
      setTimeout(() => {
        this.collectedService.updateCollection(this.pokemonName)
        .subscribe({
        next: (response: User) => {
          this.isInCollection = this.userService.inCollection(this.pokemonName)
        },
        error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message);
        }
    })
        this.animationClose=false}, 1900)
    }   
  }
}
