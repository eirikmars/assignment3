import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { CataloguePage } from "./pages/catalogue/catalogue.page";
import { LoginPage } from "./pages/login/login.page";
import { SearchResultPage } from "./pages/search-result/search-result.page";
import { TrainerPage } from "./pages/trainer/trainer.page";

const routes: Routes = [
    {
        path:"",
        pathMatch:"full",
        redirectTo: "/login"
    },
    {
        path: "login",
        component: LoginPage
    },
    {
        path: "profile",
        component: TrainerPage,
        canActivate: [ AuthGuard ]

    },
    {
        path: "catalogue",
        component: CataloguePage,
        canActivate: [ AuthGuard ]
    },
    {
        path: "search-result",
        component: SearchResultPage,
        canActivate: [AuthGuard]
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}