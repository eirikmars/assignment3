export class StorageUtil {
    /**
     * This function deletes a key from local storage
     * @param {string} key - The key of the item you want to delete.
     */
    public static storageDelete(key: string) {
        localStorage.removeItem(key);
    }

    /**
     * This function takes a key and a value, and saves the value to local storage using the key.
     * @param {string} key - The key to store the value under.
     * @param {T} value - The value to be stored.
     */
    public static storageSave<T>(key: string, value: T): void{
        localStorage.setItem(key, JSON.stringify(value));
    }
    
    /**
     * If the value stored in localStorage is valid JSON, return it, otherwise return undefined.
     * @param {string} key - string - The key to store the value under.
     * @returns The value of the key in localStorage.
     */
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = localStorage.getItem(key);
        try {
            if (storedValue) {
                return JSON.parse(storedValue);
            }
            return undefined;
        } 
        catch(e) {
            localStorage.removeItem(key);
            return undefined;
        }
    }

    /**
     * It takes a generic type T, and saves it to sessionStorage as a string.
     * @param {T} value - T - The value to be saved.
     */
    public static pokemonSave<T>(value: T): void{
        sessionStorage.setItem("pokemon", JSON.stringify(value)); 
    }

    /**
     * If there is a value in sessionStorage, return it. If there isn't, return undefined. If there is
     * a value but it's not valid JSON, remove it from sessionStorage and return undefined.
     * @returns The stored value is being returned.
     */
    public static pokemonRead<T>() {
        const storedValue = sessionStorage.getItem("pokemon")
        try {
            if (storedValue) {
                return JSON.parse(storedValue);
            }
            return undefined;
        } 
        catch(e) {
            sessionStorage.removeItem("pokemon");
            return undefined;
        }
    }
}