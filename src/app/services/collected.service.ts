import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';
import { UserService } from './user.service';

const { apiKey, apiUsers } = environment;

@Injectable({
  providedIn: 'root'
})
export class CollectedService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly userService: UserService
  ) { }

  public updateCollection(pokemonName: string): Observable<User> {
    if (!this.userService.user) {
          throw new Error("addToCollection: There is no user");
        }

    const user: User = this.userService.user;

    if (this.userService.inCollection(pokemonName)) {
      this.userService.removeFromCollection(pokemonName);
      } else {
        this.userService.addToCollection(pokemonName);
      }

    const headers = new HttpHeaders({
          'content-type': 'application/json',
          'x-api-key': apiKey
        })

    this._loading = true;

    StorageUtil.storageSave("pokemon-trainer", user )

    return this.http.patch<User>(`${apiUsers}/${user.id}`, {
      pokemon: [...user.pokemon]
    }, {
      headers
    })
    .pipe(
      tap((updatedUser: User) => {
        this.userService.user = updatedUser;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }
}
