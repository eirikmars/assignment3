import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User;
  private _pokemons: Pokemon[] = [];

  get user(): User | undefined {
    return this._user;
  }

  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!)
    this._user = user;
  }

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  constructor() {
    this._user = StorageUtil.storageRead<User>(StorageKeys.User)

  }

  /**
   * If the user exists, return true if the pokemonName is in the user's pokemon collection. Otherwise,
   * return false
   * @param {string} pokemonName - string - the name of the pokemon you want to check if it's in the
   * collection
   * @returns A boolean value.
   */
  public inCollection(pokemonName: string): boolean {
    if (this._user) {
      return Boolean(this.user?.pokemon.find((name: string) => name === pokemonName))
    }
    return false;
  }

  /**
   * If the user exists, add the pokemonName to the user's pokemon collection
   * @param {string} pokemonName - string - The name of the pokemon to add to the collection.
   */
  public addToCollection(pokemonName: string): void {
    if (this._user) {
      this._user.pokemon.push(pokemonName);
    }
  }

  /**
   * If the user exists, then set the user's pokemon to the user's pokemon, but filter out the pokemon
   * that matches the pokemonName argument.
   * @param {string} pokemonName - string - The name of the pokemon to remove from the collection
   */
  public removeFromCollection(pokemonName: string): void {
    if (this._user) {
      this._user.pokemon = this._user.pokemon
      .filter((name: string) => name !== pokemonName);
    }
  }
}
