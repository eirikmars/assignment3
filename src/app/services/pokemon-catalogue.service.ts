import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.util';

const { apiPokemon } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _searchPokemons: Pokemon[] = [];
  private _error: string = ";"
  private _loading: boolean = false;
  private _pokeIndex: number = 0;
  private _pokemonPerPage: number = 24;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  set pokemons(pokemons: Pokemon[]) {
    this._pokemons = pokemons;
  }

  get searchPokemons(): Pokemon[] {
    return this._searchPokemons;
  }

  set searchPokemons(pokemons: Pokemon[]) {
    this._searchPokemons = pokemons;
  }

  get pokemonPerPage(): number {
    return this._pokemonPerPage;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  get pokeIndex(): number {
    return this._pokeIndex;
  }
  
  set pokeIndex(index: number) {
    this._pokeIndex = index
  }

  get allPokemons(): Pokemon[] {
    return StorageUtil.pokemonRead();
  }
  
  constructor(private readonly http: HttpClient) { }

  /**
   * This function updates the pokemons array by reading the pokemons from the storage and slicing the
   * array to get the pokemons that are to be displayed on the current page.
   */
  private updatePokemons(): void {
    this._pokemons = StorageUtil.pokemonRead().slice(this._pokeIndex, this._pokeIndex+this._pokemonPerPage)
  }

  /**
   * It sets the _pokeIndex to 0 and then calls the updatePokemons() function.
   */
  public findFirstPokemons(): void {
    this._pokeIndex = 0
    this.updatePokemons()
  }

 /**
  * It takes a page number as an argument, and then sets the _pokeIndex to the index of the first
  * pokemon on that page. 
  * 
  * The updatePokemons() function is called to update the displayed pokemon.
  * @param {number} page - number - the page number that the user clicked on
  */
  public findPokemonsOnPage(page: number): void {
    this._pokeIndex = (page-1)*this._pokemonPerPage
    this.updatePokemons()
  }
  
  /**
   * It takes the current index of the pokemon and adds the number of pokemon per page to it. Then it
   * calls the updatePokemons function.
   */
  public findNextPokemons(): void {
    this._pokeIndex += this._pokemonPerPage
    this.updatePokemons()
  }

  /**
   * It takes the current index of the pokemon and subtracts the number of pokemon per page from it.
   * Then it calls the updatePokemons function.
   */
  public findPreviousPokemons(): void {
    this._pokeIndex -= this._pokemonPerPage
    this.updatePokemons()
  }
  
  /**
   * It's a function that makes a request to the pokeAPI, and then saves the response to local storage.
   */
  public findAllPokemons(): void {
    this._loading = true;
    this.http.get<any>(apiPokemon)
    .pipe(
      finalize(() => {
        this._loading = false;
        this.updatePokemons()
      })
      )
      .subscribe({
        next: (pokemon: any) => {
          const poke: Pokemon[] = pokemon.results;
          poke.forEach(element => {
            element.id = element.url.split("/")[6]
          });
          StorageUtil.pokemonSave<Pokemon[]>(poke)
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })  
  }

  /**
   * It returns the first Pokemon in the array that has a name that matches the name passed in as an
   * argument
   * @param {string} name - string - the name of the pokemon you want to find
   * @returns The pokemon object with the name that matches the name passed in.
   */
  public findPokemonByName(name: string): Pokemon | undefined {
    return this.allPokemons.find((pokemon: Pokemon) => pokemon.name === name)
  }
  
}
