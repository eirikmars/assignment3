import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit{

  constructor( private readonly router: Router) { }

  handleLogin(): void {
    this.router.navigateByUrl("/catalogue");
  }

  /**
   * If the user has already logged in, then redirect them to the catalogue page
   */
  ngOnInit(): void {
    if(StorageUtil.storageRead("pokemon-trainer") != undefined) {
      this.router.navigateByUrl("/catalogue");
    }
  }

}
