import { Component, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  constructor(
    private pokemonCatalogueService: PokemonCatalogueService,
    private userService: UserService
    ) {
    }

  get user(): User {
    return this.userService.user!;
  }

  /**
   * For each pokemon name in the user's pokemon array, find the pokemon in the catalogue and add it
   * to the user's pokemon array.
   * @returns An array of Pokemon objects.
   */
  get pokemons(): Pokemon[] {
    const userPokemons: Pokemon[] = []
    for(const name of this.user.pokemon) {
      const pokemon: Pokemon | undefined = this.pokemonCatalogueService.findPokemonByName(name)
      if (pokemon) {
        userPokemons.push(pokemon)
      }
    }
    return userPokemons
  }
  ngOnInit(): void {

  }

}
