import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.page.html',
  styleUrls: ['./search-result.page.css']
})
export class SearchResultPage implements OnInit {
  
  get pokemons(): Pokemon[] {
    return this.pokemonCatalogueService.searchPokemons;
  }

  get pokemonsLength(): number {
    return this.pokemons.length
  }

  get loading(): boolean {
    return this.pokemonCatalogueService.loading
  }

  constructor(
    private pokemonCatalogueService : PokemonCatalogueService,
    private router : Router
  ) { }
  
  returnToCatalogue() {
    this.router.navigateByUrl("/catalogue")
  }

  ngOnInit(): void {
  }

}
