# NgTrainPokemons

An angular application that allows a user to collect Pokémon received from the PokeAPI.

## Login page

The user will first be greeted with a login page, where the user must enter a username before being able to collect any Pokémon.

## Catalogue page

After a successful login, the user will be sent to the catalogue page. Here the user can catch pokemon and release cought pokemons.

## Trainer page

On the trainer page, you can see all your collected pokemons, logout and release cought pokemon.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Maintained by:
- [Eirik Marstrander](https://www.gitlab.com/eirikmars) 
- [Emma Kaulum Sanding](https://www.gitlab.com/emmakaulum)
