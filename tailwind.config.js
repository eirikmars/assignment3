/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{ts,html}'
  ],
  theme: {
    extend: {
      keyframes: {
        shake: {
          '0%': { transform: 'rotate(0.0deg)' },
          '10%': { transform: 'rotate(14deg)' },
          '20%': { transform: 'rotate(-8deg)' },
          '30%': { transform: 'rotate(14deg)' },
          '40%': { transform: 'rotate(-4deg)' },
          '50%': { transform: 'rotate(10.0deg)' },
          '60%': { transform: 'rotate(0.0deg)' },
          '100%': { transform: 'rotate(0.0deg)' },
        },
      },
      animation: {
        'shake-ball': 'shake 1s linear infinite',
      },
      
    },
  },
  plugins: [],
}